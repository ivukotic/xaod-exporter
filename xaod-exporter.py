import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from pyspark.sql import functions as F
from pyspark.sql.types import *


from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan, bulk

es = Elasticsearch(['atlas-kibana.mwt2.org:9200'], timeout=60)
es.ping()

df = spark.read.json("/user/rucio01/traces/traces.2019-10-23*")
df.printSchema()