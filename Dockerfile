FROM gitlab-registry.cern.ch/db/cerndb-infra-hadoop-conf:master
MAINTAINER Ilija Vukotic
ARG git_user
ARG git_psswd

# Add the user UID:1000, GID:1000, home at /app
# RUN groupadd -r ivukotic -g 1000 && useradd -u 1000 -r -g frntmon -m -d /home/frntmon -s /sbin/nologin -c "Frontier user" frntmon && chmod 755 /home/frntmon

# set up environment variables
ENV USR ivukotic
WORKDIR /home/${USR}


RUN yum install -y \
    httpd \
    rsync \
    libaio \
    python-pip \
    python-devel \
    python3 \
    python3-devel

# Python packages
#RUN pip3 install --no-cache-dir "git+https://${git_user}:${git_psswd}@gitlab.cern.ch/formica/coolR.git#egg=coolr&subdirectory=coolR-client/python"

#install analytics packages
# COPY Analytics /home/${USR}/Analytics
# copy templates and static files into the workDIR
COPY xaod-exporter.py /home/${USR}/
COPY run.sh /home/${USR}/

RUN pip install --upgrade pip
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir \
    h5py \
    tables \
    numpy \
    pandas \
    scipy \
    sklearn \
    elasticsearch \
    matplotlib \
    pyspark \
    seaborn

# ENV C_FORCE_ROOT true
# ENV HOST 0.0.0.0
# ENV PORT 5000
# ENV DEBUG true

# CMD ["python3", "xaod-exporter.py"]
CMD ["./run.sh"]
